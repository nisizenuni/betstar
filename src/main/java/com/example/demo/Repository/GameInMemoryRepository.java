package com.example.demo.Repository;


import com.example.demo.DataHolder.DataHolder;
import com.example.demo.Model.Game;
import org.apache.tomcat.jni.Local;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class GameInMemoryRepository {

    public List<Game> listAll(){
        return DataHolder.games;
    }

    public List<Game> findByLeague(String leagueName) {
        return  DataHolder.games.stream()
                .filter(game -> game.getHomeTeam().getLeague().equals(leagueName))
                .collect(Collectors.toList());
    }
    public List<Game> findByDate(LocalDateTime localDateTime) {
        return DataHolder.games.stream()
                .filter(game -> game.getLocalDateTime().isAfter(localDateTime))
                .collect(Collectors.toList());
    }
    public Game findById(Integer Id) {
        //TODO return an Optional If I't doesnt work like this.
        return DataHolder.games.stream().filter(game -> game.getGameId().equals(Id)).collect(Collectors.toList()).get(0);
    }
}
