package com.example.demo.Repository;


import com.example.demo.DataHolder.DataHolder;
import com.example.demo.Model.Ticket;
import com.example.demo.Model.exceptions.TicketNotFoundUserNameException;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TicketInMemoryRepository {


    public List<Ticket> listAll() {
        return DataHolder.tickets;
    }

    public Optional<Ticket> findById(Integer Id) {
        return DataHolder.tickets.stream()
                .filter(ticket -> ticket.getId()
                .equals(Id))
                .findFirst();
    }
    public Ticket findByObject(Ticket ticket) {
        return DataHolder.tickets.stream().filter(ticket1 -> ticket1.getUser().getUsername().equals(ticket.getUser().getUsername())).findFirst()
                .orElseThrow(() -> new TicketNotFoundUserNameException(ticket.getUser().getUsername()));
    }
    public Optional<Ticket> findByUsername(String username) {
        return DataHolder.tickets
                .stream().filter(ticket -> ticket.getUser()
                .getUsername().equalsIgnoreCase(username))
                .findFirst();
    }

    public Ticket save(Ticket ticket) {
        DataHolder.tickets
                .removeIf(ticket1 -> ticket1.getUser().getUsername().equals(ticket.getUser().getUsername()));
          DataHolder.tickets.add(ticket);
          return ticket;
    }
    public void deleteById(Integer ticketId){
       DataHolder.tickets
               .removeIf(ticket -> ticket.getId()
                       .equals(ticketId));
    }
}
