package com.example.demo.Repository;

import com.example.demo.DataHolder.DataHolder;
import com.example.demo.Model.Team;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class TeamInMemoryRepository {
    public  List<Team> listAll() {
        return DataHolder.teams;
    }
    public Optional<Team> findTeam(String teamName) {
        return DataHolder.teams.stream().filter(team -> team.getTeamName().equalsIgnoreCase(teamName)).findFirst();
    }
}
