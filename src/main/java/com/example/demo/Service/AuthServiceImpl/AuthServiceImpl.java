package com.example.demo.Service.AuthServiceImpl;


import com.example.demo.Model.User;
import com.example.demo.Model.exceptions.InvalidAgeException;
import com.example.demo.Model.exceptions.InvalidArgumentsException;
import com.example.demo.Model.exceptions.PasswordDoesntMatchException;
import com.example.demo.Repository.UserInMemoryRepository;
import com.example.demo.Service.AuthService;
import org.springframework.stereotype.Service;

@Service
public class AuthServiceImpl implements AuthService {

    private final UserInMemoryRepository userInMemoryRepository;

    public AuthServiceImpl(UserInMemoryRepository userInMemoryRepository) {
        this.userInMemoryRepository = userInMemoryRepository;
    }

    @Override
    public User login(String username, String password) {
        if(username.isEmpty()|| password.isEmpty())
            throw new InvalidArgumentsException();
        return userInMemoryRepository
                .findByUserNameAndPassword(username,password)
                .orElseThrow(InvalidArgumentsException::new);
         }

    @Override
    //UserName Password RepeatedPassword ,email ,age
    public User register(String username,String password,String repeatedPassword,String email,Integer age) {
        if(username.isEmpty() || age == null || password.isEmpty() || email.isEmpty() || repeatedPassword.isEmpty())
            throw new InvalidArgumentsException();
        if(age<18)
            throw  new InvalidAgeException();
        if(!password.matches(repeatedPassword))
            throw new PasswordDoesntMatchException();

       User user = new User(username,age,password);
       return userInMemoryRepository.saveOrUpdate(user);
    }


}
