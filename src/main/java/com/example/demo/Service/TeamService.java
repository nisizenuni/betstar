package com.example.demo.Service;

import com.example.demo.Model.Team;

import java.util.List;
import java.util.Optional;

public interface TeamService {
    List<Team> listAll();
    Optional<Team> findTeam(String teamName);
}
