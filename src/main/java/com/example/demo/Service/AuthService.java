package com.example.demo.Service;


import com.example.demo.Model.User;


//String username ,Integer age, String password
public interface AuthService {
    User login(String username,String password);
    //UserName Password RepeatedPassword ,email ,age
    User register(String username,String password,String repeatedPassword,String email,Integer age);
}
