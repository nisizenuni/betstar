package com.example.demo.Service;

import com.example.demo.Model.Game;
import com.example.demo.Model.Ticket;

import java.util.List;
import java.util.Optional;

public interface TicketService {
    List<Ticket> listAllTickets();
    Optional<Ticket> findTicketByUser(String username);
    Optional<Ticket> findTicketById(Integer Id);
    Ticket addGameToTicket(String username,Integer gameId);
    Ticket editGameInTicket(Integer ticketId);
    List<Game> listGamesInTicket(Integer ticketId);
    List<Game> listGamesInTicketByUser(String user);
    Ticket findTicketByObject(Ticket ticket);
    void deleteById(Integer ticketId);
}
