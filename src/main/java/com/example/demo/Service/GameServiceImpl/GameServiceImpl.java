package com.example.demo.Service.GameServiceImpl;

import com.example.demo.Model.Game;
import com.example.demo.Repository.GameInMemoryRepository;
import com.example.demo.Service.GameService;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class GameServiceImpl implements GameService {

    private final GameInMemoryRepository gameInMemoryRepository;

    public GameServiceImpl(GameInMemoryRepository gameInMemoryRepository) {
        this.gameInMemoryRepository = gameInMemoryRepository;
    }


    @Override
    public List<Game> listAll() {
        return this.gameInMemoryRepository.listAll();
    }

    @Override
    public List<Game> filterByLeague(String leagueName) {
        return this.gameInMemoryRepository.findByLeague(leagueName);
    }

    @Override
    public List<Game> filterByDate(LocalDateTime localDateTime) {
        return this.gameInMemoryRepository.findByDate(localDateTime);
    }

}
