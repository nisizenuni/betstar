package com.example.demo.Service.TeamServiceImpl;

import com.example.demo.Model.Team;
import com.example.demo.Repository.TeamInMemoryRepository;
import com.example.demo.Service.TeamService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TeamServiceImpl implements TeamService {

    private final TeamInMemoryRepository teamInMemoryRepository;

    public TeamServiceImpl(TeamInMemoryRepository teamInMemoryRepository) {
        this.teamInMemoryRepository = teamInMemoryRepository;
    }


    @Override
    public List<Team> listAll() {
        return this.teamInMemoryRepository.listAll();
    }

    //TODO find team so u can show information me grafe sene vene
    @Override
    public Optional<Team> findTeam(String teamName) {
        return this.teamInMemoryRepository.findTeam(teamName);
    }
}
