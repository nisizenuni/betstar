package com.example.demo.Service;

import com.example.demo.Model.Game;

import java.time.LocalDateTime;
import java.util.List;

public interface GameService {
    public List<Game> listAll();
    public List<Game> filterByLeague(String leagueName);
    public List<Game> filterByDate(LocalDateTime localDateTime);
}
