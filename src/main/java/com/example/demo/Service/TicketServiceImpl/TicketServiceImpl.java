package com.example.demo.Service.TicketServiceImpl;

import com.example.demo.Model.Game;
import com.example.demo.Model.Ticket;
import com.example.demo.Model.exceptions.GameAlreadyInTicketException;
import com.example.demo.Model.exceptions.TicketNotFoundException;
import com.example.demo.Model.exceptions.TicketNotFoundUserNameException;
import com.example.demo.Repository.GameInMemoryRepository;
import com.example.demo.Repository.TicketInMemoryRepository;
import com.example.demo.Repository.UserInMemoryRepository;
import com.example.demo.Service.TicketService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;



@Service
public class TicketServiceImpl implements TicketService {


    private final TicketInMemoryRepository ticketInMemoryRepository;
    private final GameInMemoryRepository gameInMemoryRepository;
    private final UserInMemoryRepository userInMemoryRepository;

    public TicketServiceImpl(TicketInMemoryRepository ticketInMemoryRepository, GameInMemoryRepository gameInMemoryRepository, UserInMemoryRepository userInMemoryRepository) {
        this.ticketInMemoryRepository = ticketInMemoryRepository;
        this.gameInMemoryRepository = gameInMemoryRepository;
        this.userInMemoryRepository = userInMemoryRepository;
    }

    @Override
    public List<Ticket> listAllTickets() {
        return this.ticketInMemoryRepository.listAll();
    }

    @Override
    public Optional<Ticket> findTicketByUser(String username) {
        return this.ticketInMemoryRepository.findByUsername(username);
    }

    //TODO IMPLEMENT EDIT FUNCTIONALITY

    @Override
    public Optional<Ticket> findTicketById(Integer Id) {
        return this.ticketInMemoryRepository.findById(Id);
    }

    @Override
    public Ticket addGameToTicket(String username, Integer gameId) {
        Game gameToAdd  = this.gameInMemoryRepository.findById(gameId);
        //TODO exception is with ID
        Ticket ticket = this.ticketInMemoryRepository.findByUsername(username)
                .orElseThrow(() -> new TicketNotFoundUserNameException(username));
       if(ticket.getGameList().stream().anyMatch(game -> game.getGameId().equals(gameId)))
           throw  new GameAlreadyInTicketException(gameId);
        ticket.getGameList().add(gameToAdd);
       return ticketInMemoryRepository.save(ticket);
    }

    @Override
    public Ticket editGameInTicket(Integer ticketId) {
        return null;
    }

    @Override
    //TODO KINDA USELESS MAYBE DO IT WITH AN OBJECT
    public List<Game> listGamesInTicket(Integer ticketId) {
        if(!this.ticketInMemoryRepository.findById(ticketId).isPresent())
            throw  new TicketNotFoundException(ticketId);
       return this.ticketInMemoryRepository.findById(ticketId).get().getGameList();
    }

    @Override
    public List<Game> listGamesInTicketByUser(String user) {
        if(!this.ticketInMemoryRepository.findByUsername(user).isPresent())
            throw new TicketNotFoundUserNameException(user);
        return this.ticketInMemoryRepository.findByUsername(user).get().getGameList();
    }

    @Override
    public Ticket findTicketByObject(Ticket ticket) {
        return this.ticketInMemoryRepository.findByObject(ticket);
    }


    @Override
    public void deleteById(Integer ticketId) {
         this.ticketInMemoryRepository.deleteById(ticketId);
    }
}
