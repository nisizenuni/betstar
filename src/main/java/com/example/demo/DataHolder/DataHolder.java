package com.example.demo.DataHolder;

import com.example.demo.Model.Game;
import com.example.demo.Model.Team;
import com.example.demo.Model.Ticket;
import com.example.demo.Model.User;
import lombok.Getter;
import org.springframework.stereotype.Component;


import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Component
@Getter
public class DataHolder {

    public static List<User> users = new ArrayList<>();
    public static List<Team> teams = new ArrayList<>();
    public static List<Game> games = new ArrayList<>();
    public static List<Ticket> tickets = new ArrayList<>();
    public static List<Team> laligateams = new ArrayList<>();
    public static List<Team> seriaAteams = new ArrayList<>();
    public static List<Team> ligue1teams = new ArrayList<>();


    @PostConstruct
    public void init() {

        users.add(new User("Enis",21,"enis"));
        users.add(new User("Rrezart",21,"rrezart"));

        User Enis = new User("Enis",23,"enis123");
        User Rrezart = new User("Rrezart",24,"rrezart123");


        teams.add(new Team("Premier League",1,"City",63,20,3,2));
        teams.add(new Team("Premier League",2,"Liverpool",60,19,3,3));
        teams.add(new Team("Premier League",3,"Chelsea",57,18,3,4));
        teams.add(new Team("Premier League",4,"West Ham",54,17,3,5));
        teams.add(new Team("Premier League",5,"United",51,16,3,6));

        //PL teams -- RACNO
        Team City = new Team("Premier League",1,"City",63,20,3,2);
        Team Liverpool = new Team("Premier League",2,"Liverpool",60,19,3,3);
        Team Chelsea = new Team("Premier League",3,"Chelsea",57,18,3,4);
        Team WestHam = new Team("Premier League",4,"West Ham",54,17,3,5);
        Team United = new Team("Premier League",5,"United",51,16,3,6);

        //La Liga teams -- Racno
        Team RealMadrid = new Team("La Liga",1,"Real Madrid",66,21,3,1);
        Team Sevilla = new Team("La Liga",2,"Sevilla",63,21,3,2);
        Team Barcelona = new Team("La Liga",3,"Barcelona",60,20,3,3);
        Team Atletico = new Team("La Liga",4,"Atletico",57,19,3,3);
        Team Betis = new  Team("La Liga",5,"Betis",54,21,3,4);

        laligateams.add(RealMadrid);
        laligateams.add(Sevilla);
        laligateams.add(Barcelona);
        laligateams.add(Atletico);
        laligateams.add(Betis);

        //Seria A teams -- Racno
        Team Milan = new Team("Seria A ",1,"Milan",66,21,3,1);
        Team Inter = new Team("Seria A",2,"Inter",63,21,3,2);
        Team Juventus = new Team("Seria A ",3,"Juventus",60,20,3,3);
        Team Roma = new Team("Seria A",4,"Roma",57,19,3,3);
        Team Napoli =  new Team("Seria A",5,"Napoli",54,21,3,4);

        seriaAteams.add(Milan);
        seriaAteams.add(Inter);
        seriaAteams.add(Juventus);
        seriaAteams.add(Roma);
        seriaAteams.add(Napoli);

        //Ligue 1 teams - Racno
        Team PSG = new Team("Ligue 1",1,"PSG",66,21,3,1);
        Team Marseille = new Team("Ligue 1",2,"Marseille",63,21,3,2);
        Team Lyon = new Team("Ligue 1",3,"Lyon",60,20,3,3);
        Team Nantes = new Team("Ligue 1",4,"Nantes",57,19,3,3);
        Team Lille =  new Team("Ligue 1",5,"Lille",54,21,3,4);

        ligue1teams.add(PSG);
        ligue1teams.add(Marseille);
        ligue1teams.add(Lyon);
        ligue1teams.add(Nantes);
        ligue1teams.add(Lille);


        //Populating games
        // public Game(Integer gameId, LocalDateTime localDateTime, Team homeTeam, Team awayTeam)
        games.add(new Game(1, LocalDateTime.now(),Liverpool,City));
        games.add(new Game(2,LocalDateTime.now(),Chelsea,United));
        games.add(new Game(3,LocalDateTime.now(),WestHam,Liverpool));
        games.add(new Game(4,LocalDateTime.now(),Chelsea,WestHam));

        //Creating game objects to add them in array List
        Game game1 = new Game(1, LocalDateTime.now(),Liverpool,City);
        Game game2 = new Game(2, LocalDateTime.now(),Chelsea,WestHam);
        Game game3 = new Game(3,LocalDateTime.now(),Liverpool,Chelsea);
        Game game4 = new Game(4,LocalDateTime.now(),City,WestHam);
        Game game5 = new Game(5,LocalDateTime.now(),Chelsea,United);

        //Games ArrayList nameneto za Tickets
        List<Game> games = new ArrayList<>();
        games.add(game1);
        games.add(game2);
        games.add(game3);
        games.add(game4);
        games.add(game5);

        //Tickets Data
        tickets.add(new Ticket(1,LocalDateTime.now(),Enis,games));
        tickets.add(new Ticket(2,LocalDateTime.now(),Rrezart,games));

    }

}
