package com.example.demo.Model;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class User {
    String username;
    Integer age;
    String password;


//TODO add Repeated Password && CHECK IF USER IS OVER 18
    public User(String username ,Integer age, String password) {
        this.username = username;
        this.age = age;
        this.password = password;
    }
}

