package com.example.demo.Model.exceptions;

public class InvalidAgeException  extends RuntimeException {
    public InvalidAgeException() {
        super("Users under 18 are not allowed exception");
    }
}
