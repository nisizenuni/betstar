package com.example.demo.Model.exceptions;

public class PasswordDoesntMatchException extends RuntimeException{
    public PasswordDoesntMatchException() {
        super("Password Doesn't Match Exception");
    }
}
