package com.example.demo.Model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.PRECONDITION_FAILED)
public class GameAlreadyInTicketException extends RuntimeException{
    public GameAlreadyInTicketException(Integer gameId) {
        super(String.format("Game with Id %d is already added",gameId));
    }
}


