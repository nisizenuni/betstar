package com.example.demo.Model.exceptions;

public class TicketNotFoundUserNameException extends RuntimeException {
    public TicketNotFoundUserNameException(String username) {
        super(String.format("Ticket of user %s not found",username));
    }
}
