package com.example.demo.Model.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TicketNotFoundException extends RuntimeException{
      public TicketNotFoundException(Integer Id) {
          super(String.format("Ticket with the Id %d was not found.Please Try Again!",Id));
      }
}
