package com.example.demo.Model;

import lombok.Data;
import lombok.Getter;

@Data
@Getter
public class Team {
    String league;
    Integer position;
    String teamName;
    Integer points;
    Integer gamesWon;
    Integer gamesDraw;
    Integer gamesLost;

    //TODO remove the points do the formula 3*gamesWon + 1*gameDrawn
    public Team(String league, Integer position, String teamName, Integer points, Integer gamesWon, Integer gamesDraw, Integer gamesLost) {
        this.league = league;
        this.position = position;
        this.teamName = teamName;
        this.points = points;
        this.gamesWon = gamesWon;
        this.gamesDraw = gamesDraw;
        this.gamesLost = gamesLost;
    }

    public String getLeague() {
        return league;
    }
}
