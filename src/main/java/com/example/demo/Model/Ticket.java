package com.example.demo.Model;


import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class Ticket {
    private Integer Id;
    private LocalDateTime localDateTime;
    private User user;
    private List<Game> gameList;


    public Ticket(Integer id, LocalDateTime localDateTime, User user, List<Game> gameList) {
        this.Id = id; //TODO BE A PRIMARY KEY WHEN PERSISTING
        this.localDateTime = localDateTime;
        this.user = user;
        this.gameList = gameList;
    }
}
