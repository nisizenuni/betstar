package com.example.demo.Model;

import lombok.Data;
import lombok.Getter;

import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.Date;

@Data
@Getter
public class Game {
    Integer gameId;
    LocalDateTime localDateTime;
    Team homeTeam;
    Team awayTeam;
    String tipType;
    Double coef1;
    Double coefx;
    Double coef2;


    public Game(Integer gameId, LocalDateTime localDateTime, Team homeTeam, Team awayTeam) {
        this.gameId = gameId;
        this.localDateTime = localDateTime;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
       // TODO implement coeficient based on position and Math.Random on closed intervals - abs value between positions
        coef1 = Math.ceil(((Math.random() * 1.90) + 1)*100)/100;
        coefx = Math.ceil(((Math.random() * 2.85) + 1)*100)/100;
        coef2 = Math.ceil(((Math.random() *4.50) + 1)*100)/100;

    }
}
