package com.example.demo.WebControllers;


import com.example.demo.Model.Game;
import com.example.demo.Service.GameService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/bet")
public class GameController {

    private final GameService gameService;

    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    public String showBet(Model model) {
        List<Game> games = gameService.listAll();
        model.addAttribute("games",games);
        return "bet";
    }

}
