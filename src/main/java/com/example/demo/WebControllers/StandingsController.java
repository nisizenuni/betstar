package com.example.demo.WebControllers;
import com.example.demo.Model.Team;
import com.example.demo.Service.TeamService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@org.springframework.stereotype.Controller
@RequestMapping("/standings")
public class StandingsController {

    private final TeamService teamService;

    public StandingsController(TeamService teamService) {
        this.teamService = teamService;
    }


    @GetMapping
    public String getStandings(Model model) {
        List<Team> teams = this.teamService.listAll();
        model.addAttribute("teams",teams);

       // model.addAttribute("teams",teams);
        return "standings";
    }



}
