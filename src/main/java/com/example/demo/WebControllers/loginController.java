package com.example.demo.WebControllers;


import com.example.demo.Model.User;
import com.example.demo.Model.exceptions.InvalidArgumentsException;
import com.example.demo.Service.AuthService;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;


@Controller
@RequestMapping("/login")
public class loginController {
    private final AuthService authService;

    public loginController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping
    public String returnLogIn() {
        return "login";
    }

    @PostMapping
    public String login(@RequestParam String username, @RequestParam String password, @NotNull Model model, @NotNull HttpServletRequest request) {

        User user = null;
        try {
            user = this.authService.login(username, password);
            model.addAttribute("User", user);
            request.getSession().setAttribute("user",user);
            return "redirect:/home";
        } catch (InvalidArgumentsException exception) {

            model.addAttribute("hasError", true);
            model.addAttribute("error", exception.getMessage());
            return "login";
        }
    }
}
