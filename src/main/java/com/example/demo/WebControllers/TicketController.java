package com.example.demo.WebControllers;


import com.example.demo.Model.Ticket;
import com.example.demo.Model.User;
import com.example.demo.Model.exceptions.TicketNotFoundUserNameException;
import com.example.demo.Service.TicketServiceImpl.TicketServiceImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/ticket")
public class TicketController {
    private final TicketServiceImpl ticketService;


    public TicketController(TicketServiceImpl ticketService) {
        this.ticketService = ticketService;
    }

    @GetMapping
    public String getTicketView(@RequestParam(required = false) String error, HttpServletRequest request, Model model) {
       if(error!=null&&error.isEmpty()) {
           model.addAttribute("hasError",true);
           model.addAttribute("error",error);
       }
        User user = (User) request.getSession().getAttribute("user");
        Ticket ticket = this.ticketService
                .findTicketByUser(user.getUsername())
                .orElseThrow(() -> new TicketNotFoundUserNameException(user.getUsername()));
        model.addAttribute("gamesInTicket",this.ticketService.listGamesInTicket(ticket.getId())); //Games in TIcket for view
        model.addAttribute("ticket",ticket); //Ticket for view
        model.addAttribute("user",user); //User for view
        model.addAttribute("referer",request.getHeader("Referer"));

  return "ticket";
    }

    @DeleteMapping("/delete/{gameId}")
    public String deleteGameFromTicket(@PathVariable Integer gameId) {
      this.ticketService.deleteById(gameId);
      return "redirect:/ticket";
    }
}
