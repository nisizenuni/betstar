package com.example.demo.WebControllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

//TODO href linket in the boxes


@Controller
@RequestMapping("/home")
public class HomeController {

    @GetMapping
    public String getBetStar() {
        return "betstar";
    }
}
