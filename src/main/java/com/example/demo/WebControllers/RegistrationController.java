package com.example.demo.WebControllers;

import com.example.demo.Model.User;
import com.example.demo.Model.exceptions.InvalidAgeException;
import com.example.demo.Model.exceptions.InvalidArgumentsException;
import com.example.demo.Service.AuthService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/register")

public class RegistrationController {
    public final AuthService authService;

    public RegistrationController(AuthService authService) {
        this.authService = authService;
    }


    @GetMapping
    public String getRegisterPage() {
        return "registration";
    }


    @PostMapping
    public String register(@RequestParam String Username,
                           @RequestParam String Password,
                           @RequestParam String repeatedPassword,
                           @RequestParam String Email,
                           @RequestParam Integer Age,
                           Model model) {
        User newUser = null;
        try {
            newUser = this.authService.register(Username, Password, repeatedPassword, Email, Age);
            model.addAttribute("newUser",newUser);
            return "redirect:/login";
        } catch (InvalidArgumentsException | InvalidAgeException exception) {
            model.addAttribute("hasError",true);
            //return "redirect:/register?error=" + exception.getMessage();
            return "registration";
        }
    }
}
